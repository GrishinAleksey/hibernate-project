package com.grishin.utils;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtils {

    private static SessionFactory sessionFactory;

    public HibernateSessionFactoryUtils() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Client.class);
                configuration.addAnnotatedClass(Passport.class);
                configuration.addAnnotatedClass(Transfer.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (Exception e) {
                System.out.println("Исключение" + e);
            }
        }
        return sessionFactory;
    }
}
