package com.grishin.repository;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
    List<Transfer> findByClient_Passport(Passport passport);
}
