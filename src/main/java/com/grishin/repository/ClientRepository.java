package com.grishin.repository;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends JpaRepository<Client, Long> {
//    @Query("select b from Passport b where b.passportData = :passport")

    Client findByPassport(Passport passport);

}
