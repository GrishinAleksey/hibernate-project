package com.grishin;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;
import com.grishin.repositoryservice.ClientRepositoryService;
import com.grishin.repositoryservice.TransferRepositoryService;
import com.grishin.repositoryservice.impl.TransferRepositoryServiceImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RepositoryTest {
    public static void main(String[] args) {
        SpringApplication.run(RepositoryTest.class, args);
    }

    @Bean
    public CommandLineRunner mappingDemo(ClientRepositoryService clientRepositoryService,
                                         TransferRepositoryService transferRepositoryService,
                                         TransferRepositoryServiceImpl transferRepositoryServiceImpl) {
        return args -> {

            Passport passport = new Passport("простой1");
            Client client = new Client("Антон1", 900D, passport);
            Transfer transfer = new Transfer(client,12D);
            clientRepositoryService.save(client);
            transferRepositoryService.save(transfer);
            transferRepositoryService.findByClient_Passport(passport);
            transferRepositoryServiceImpl.findByClient_Passport(passport);
            clientRepositoryService.findByPassport(passport);
        };
    }
}
