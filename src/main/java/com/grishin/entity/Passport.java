package com.grishin.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "passports")
public class Passport implements Serializable {

    @Id
    @Column(name = "passport_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "passport_data")
    private String passportData;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "passport")
    private Client client;

    public Passport() {
    }

    public Passport(String passportData) {
        this.passportData = passportData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassportData() {
        return passportData;
    }

    public void setPassportData(String passportData) {
        this.passportData = passportData;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
