package com.grishin.service;

import com.grishin.entity.Passport;

import java.util.List;
import java.util.Optional;

public interface PassportService {

    Passport findById(Long id);

    void save(Passport passport);

    void upgrade(Passport passport);

    void delete(Passport passport);

    List<Passport> findAll();
}
