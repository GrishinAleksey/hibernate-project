package com.grishin.service;

import com.grishin.entity.Client;

import java.util.List;

public interface ClientService {
    Client findById(Long id);

    void save(Client client);

    void upgrade(Client client);

    void delete(Client client);

    List<Client> findAll();
}
