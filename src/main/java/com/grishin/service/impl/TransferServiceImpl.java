package com.grishin.service.impl;

import com.grishin.dao.impl.TransferDaoImpl;
import com.grishin.entity.Transfer;
import com.grishin.service.TransferService;

import java.util.List;

public class TransferServiceImpl implements TransferService {

    private final TransferDaoImpl transferDao = new TransferDaoImpl();

    public TransferServiceImpl() {
    }

    @Override
    public Transfer findById(Long id) {
        return transferDao.findById(id);
    }

    @Override
    public void save(Transfer transfer) {
        transferDao.save(transfer);
    }

    @Override
    public void upgrade(Transfer transfer) {
        transferDao.upgrade(transfer);
    }

    @Override
    public void delete(Transfer transfer) {
        transferDao.delete(transfer);
    }

    @Override
    public List<Transfer> findAll() {
        return transferDao.findAll();
    }
}
