package com.grishin.service.impl;

import com.grishin.dao.impl.ClientDaoImpl;
import com.grishin.entity.Client;
import com.grishin.service.ClientService;

import java.util.List;

public class ClientServiceImpl implements ClientService {

    private final ClientDaoImpl clientDao = new ClientDaoImpl();

    public ClientServiceImpl() {
    }

    @Override
    public Client findById(Long id) {
        return clientDao.findById(id);
    }

    @Override
    public void save(Client client) {
        clientDao.save(client);
    }

    @Override
    public void upgrade(Client client) {
        clientDao.upgrade(client);
    }

    @Override
    public void delete(Client client) {
        clientDao.delete(client);
    }

    @Override
    public List<Client> findAll() {
        return clientDao.findAll();
    }

}
