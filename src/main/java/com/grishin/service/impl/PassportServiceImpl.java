package com.grishin.service.impl;

import com.grishin.dao.impl.PassportDaoImpl;
import com.grishin.entity.Passport;
import com.grishin.service.PassportService;

import java.util.List;
import java.util.Optional;

public class PassportServiceImpl implements PassportService {

    private final PassportDaoImpl passportDao = new PassportDaoImpl();

    public PassportServiceImpl() {
    }

    @Override
    public Passport findById(Long id) {
        return passportDao.findById(id);
    }

    @Override
    public void save(Passport passport) {
        passportDao.save(passport);
    }

    @Override
    public void upgrade(Passport passport) {
        passportDao.upgrade(passport);
    }

    @Override
    public void delete(Passport passport) {
        passportDao.delete(passport);
    }

    @Override
    public List<Passport> findAll() {
        return passportDao.findAll();
    }
}
