package com.grishin;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;
import com.grishin.service.impl.ClientServiceImpl;
import com.grishin.service.impl.TransferServiceImpl;

public class DaoTest {
    public static void main(String[] args) {

        ClientServiceImpl clientService = new ClientServiceImpl();
        Client client = new Client("Алексей", 123D, new Passport("Новый пасспорт"));
        clientService.save(client);
        TransferServiceImpl transferService = new TransferServiceImpl();
        Transfer transfer = new Transfer(client, 12D);
        transferService.save(transfer);
        clientService.upgrade(client);
        System.out.println(clientService.findAll());

    }
}
