package com.grishin.dao;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;

import java.util.List;

public interface PassportDao {

    Passport findById(Long id);

    void save(Passport passport);

    void upgrade(Passport passport);

    void delete(Passport passport);

    List<Passport> findAll();
}
