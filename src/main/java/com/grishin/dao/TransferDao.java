package com.grishin.dao;

import com.grishin.entity.Client;
import com.grishin.entity.Transfer;

import java.util.List;

public interface TransferDao {
    Transfer findById(Long id);

    void save(Transfer transfer);

    void upgrade(Transfer transfer);

    void delete(Transfer transfer);

    List<Transfer> findAll();
}
