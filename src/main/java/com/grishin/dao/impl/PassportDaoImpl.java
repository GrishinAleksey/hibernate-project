package com.grishin.dao.impl;

import com.grishin.dao.PassportDao;
import com.grishin.entity.Passport;
import com.grishin.utils.HibernateSessionFactoryUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class PassportDaoImpl implements PassportDao {
    @Override
    public Passport findById(Long id) {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().get(Passport.class, id);
    }

    @Override
    public void save(Passport passport) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(passport);
        transaction.commit();
        session.close();
    }

    @Override
    public void upgrade(Passport passport) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(passport);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Passport passport) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(passport);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Passport> findAll() {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().createQuery("from Passport ").list();
    }
}
