package com.grishin.dao.impl;

import com.grishin.dao.ClientDao;
import com.grishin.entity.Client;
import com.grishin.utils.HibernateSessionFactoryUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ClientDaoImpl implements ClientDao {
    @Override
    public Client findById(Long id) {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().get(Client.class, id);
    }

    @Override
    public void save(Client client) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(client);
        transaction.commit();
        session.close();
    }

    @Override
    public void upgrade(Client client) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(client);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Client client) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(client);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Client> findAll() {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().createQuery("from Client").list();
    }
}
