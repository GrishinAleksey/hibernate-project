package com.grishin.dao.impl;

import com.grishin.dao.TransferDao;
import com.grishin.entity.Transfer;
import com.grishin.utils.HibernateSessionFactoryUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class TransferDaoImpl implements TransferDao {
    @Override
    public Transfer findById(Long id) {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().get(Transfer.class, id);
    }

    @Override
    public void save(Transfer transfer) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(transfer);
        transaction.commit();
        session.close();
    }

    @Override
    public void upgrade(Transfer transfer) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(transfer);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(Transfer transfer) {
        Session session = HibernateSessionFactoryUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(transfer);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Transfer> findAll() {
        return HibernateSessionFactoryUtils.getSessionFactory().openSession().createQuery("from Transfer").list();
    }
}
