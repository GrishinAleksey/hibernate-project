package com.grishin.dao;

import com.grishin.entity.Client;

import java.util.List;

public interface ClientDao {

    Client findById(Long id);

    void save(Client client);

    void upgrade(Client client);

    void delete(Client client);

    List<Client> findAll();
}
