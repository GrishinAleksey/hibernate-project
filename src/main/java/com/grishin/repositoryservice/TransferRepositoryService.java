package com.grishin.repositoryservice;

import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;

import java.util.List;

public interface TransferRepositoryService {
    Transfer findById(Long id);

    void save(Transfer transfer);

    void upgrade(Transfer transfer);

    void delete(Transfer transfer);

    List<Transfer> findAll();

    List<Transfer> findByClient_Passport(Passport passport);
}
