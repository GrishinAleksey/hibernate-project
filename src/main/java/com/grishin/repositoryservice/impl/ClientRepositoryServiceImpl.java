package com.grishin.repositoryservice.impl;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;
import com.grishin.repository.ClientRepository;
import com.grishin.repositoryservice.ClientRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientRepositoryServiceImpl implements ClientRepositoryService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client findById(Long id) {
        return null;
    }

    @Override
    public void save(Client client) {
        clientRepository.saveAndFlush(client);
    }

    @Override
    public void upgrade(Client client) {
        clientRepository.saveAndFlush(client);
    }

    @Override
    public void delete(Client client) {
        clientRepository.delete(client);
    }

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client findByPassport(Passport passport) {
        return clientRepository.findByPassport(passport);
    }
}
