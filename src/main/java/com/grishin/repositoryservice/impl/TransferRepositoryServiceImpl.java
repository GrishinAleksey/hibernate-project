package com.grishin.repositoryservice.impl;

import com.grishin.entity.Passport;
import com.grishin.entity.Transfer;
import com.grishin.repository.TransferRepository;
import com.grishin.repositoryservice.TransferRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferRepositoryServiceImpl implements TransferRepositoryService {

    @Autowired
    private TransferRepository transferRepository;

    @Override
    public Transfer findById(Long id) {
        return null;
    }

    @Override
    public void save(Transfer transfer) {
        transferRepository.saveAndFlush(transfer);
    }

    @Override
    public void upgrade(Transfer transfer) {
        transferRepository.saveAndFlush(transfer);
    }

    @Override
    public void delete(Transfer transfer) {
        transferRepository.delete(transfer);
    }

    @Override
    public List<Transfer> findAll() {
        return transferRepository.findAll();
    }

    @Override
    public List<Transfer> findByClient_Passport(Passport passport) {
        return transferRepository.findByClient_Passport(passport);
    }
}
