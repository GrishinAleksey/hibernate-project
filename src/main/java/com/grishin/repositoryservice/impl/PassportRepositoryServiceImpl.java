package com.grishin.repositoryservice.impl;

import com.grishin.entity.Passport;
import com.grishin.repository.PassportRepository;
import com.grishin.repositoryservice.PassportRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassportRepositoryServiceImpl implements PassportRepositoryService {

    @Autowired
    private PassportRepository passportRepository;

    @Override
    public Passport findById(Long id) {
        return null;
    }

    @Override
    public void save(Passport passport) {
        passportRepository.saveAndFlush(passport);
    }

    @Override
    public void upgrade(Passport passport) {
        passportRepository.saveAndFlush(passport);
    }

    @Override
    public void delete(Passport passport) {
        passportRepository.delete(passport);
    }

    @Override
    public List<Passport> findAll() {
        return passportRepository.findAll();
    }
}
