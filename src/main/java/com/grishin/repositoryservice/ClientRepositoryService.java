package com.grishin.repositoryservice;

import com.grishin.entity.Client;
import com.grishin.entity.Passport;

import java.util.List;

public interface ClientRepositoryService {
    Client findById(Long id);

    void save(Client client);

    void upgrade(Client client);

    void delete(Client client);

    List<Client> findAll();

    Client findByPassport(Passport passport);
}
