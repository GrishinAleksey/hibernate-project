package com.grishin.repositoryservice;

import com.grishin.entity.Passport;

import java.util.List;

public interface PassportRepositoryService {
    Passport findById(Long id);

    void save(Passport passport);

    void upgrade(Passport passport);

    void delete(Passport passport);

    List<Passport> findAll();
}
